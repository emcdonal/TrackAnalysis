import sys
import os
import glob
import shutil
import ROOT
import argparse

def vec_str(iterable):
  vs = ROOT.std.vector( ROOT.std.string )()
  for item in iterable:
    vs.push_back(item)
  return vs
  
def selectionList(specifier, configDir):
  return vec_str([fname for fname in glob.glob(os.path.join(configDir,specifier)) if "_pre" not in fname])

def main():
  parser = argparse.ArgumentParser(description="Run an event loop algorithm.\n For more complicated options it is best to edit the script.")
  parser.add_argument("--out", default="out", help="Name of the directory in which to save output files.")
  parser.add_argument("--maxn", default=-1, type=int, help="Run on only the first maxn events.")
  parser.add_argument("--analysis", default="TrackAnalysis", help="Name of the analysis to run.")
  parser.add_argument("--local", nargs="*", default=[], help="Add local directories to run.")
  parser.add_argument("--eos", nargs="*", default=[], help="Add eos directories to run.")
  parser.add_argument("--fax", nargs="*", default=[], help="Add samples to load through fax.")
  parser.add_argument("--fname", default="*root*", help="Pattern matching to apply to filenames.")
  parser.add_argument("--sname", default="*", help="Pattern matching to apply to sample names.")
  parser.add_argument("--clear", action="store_true", help="Clear the output directory before running.")
  parser.add_argument("--maxD0", default=-1.0, type=float, help="D0 cut override for the track selection.")
  parser.add_argument("--maxZ0st", default=-1.0, type=float, help="Z0st cut override for the track selection.")

  parser.add_argument("--lsf", action="store_true", help="Run on the lsf batch system.")
  parser.add_argument("--queue", default="1nh", help="If using lsf, which queue to submit jobs to, e.g. 1nh")
  parser.add_argument("--nbatch", default=500000, type=int, help="If using lsf, how many events to submit per job.")


  args = parser.parse_args()

  # Load the libraries for all packages
  ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

  # Set up the job for xAOD access:
  ROOT.xAOD.Init().ignore()

  # create a new sample handler to describe the data files we use
  sh = ROOT.SH.SampleHandler()

  # ----------------------------------------
  # Load Input Files
  # ----------------------------------------
  
  # #todo move to better SH arguments?

  if not any((args.local, args.eos, args.fax)):
    print "No samples provided to run over. Please provide input arguments from one or more of --local --eos --fax"
    return 

  sources = args.local + args.eos + args.fax

  # Local files
  for path in args.local:
    print glob.glob(path)
    ROOT.SH.scanDir(sh, ROOT.SH.DiskListLocal(path), args.fname, args.sname)

  # EOS
  for path in args.eos:
    ROOT.SH.scanDir(sh, ROOT.SH.DiskListEOS(path, "root://eosatlas/" + path), args.fname, args.sname)

  # Through fax
  for sample in args.fax:
    ROOT.SH.scanDQ2(sh, sample)

  # ----------------------------------------
  # Set Job Options
  # ----------------------------------------

  # Options for SH
  # in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString("nc_tree", "CollectionTree")

  for sample in sh:
    print sample

  # ----------------------------------------
  # Create the Job
  # ----------------------------------------

  job = ROOT.EL.Job()
  job.sampleHandler(sh)
  job.useXAOD()
  job.options().setDouble(ROOT.EL.Job.optCacheSize, 10*1024*1024);
  job.options().setDouble(ROOT.EL.Job.optMaxEvents, args.maxn);

  # define an output ntuple 
  output = ROOT.EL.OutputStream("ntuple");
  job.outputAdd(output);
  ntuple = ROOT.EL.NTupleSvc("ntuple");
  job.algsAdd(ntuple);

  alg = getattr(ROOT, args.analysis)()
  job.algsAdd(alg)

  # Options for the Algorithm
  configDir = os.path.join(os.path.dirname(os.environ["ROOTCOREBIN"]), "dEdxAnalysis","config")
  alg.outputName = "ntuple";
  alg.maxD0 = args.maxD0
  alg.maxZ0st = args.maxZ0st

  # ----------------------------------------
  # Submit!
  # ----------------------------------------

  # make the driver we want to use:
  # this one works by running the algorithm directly:
  #driver = ROOT.EL.DirectDriver()

  # #todo Make this a command line option
  # we can use other drivers to run things on the Grid, with PROOF, etc.
  if args.lsf:
    print "Running with the LSF driver."
    driver = ROOT.EL.LSFDriver()
    ROOT.SH.scanNEvents(sh);
    sh.setMetaDouble(ROOT.EL.Job.optEventsPerWorker, args.nbatch);
    job.options().setString(ROOT.EL.Job.optSubmitFlags, "-q " + args.queue);
  else:
    driver = ROOT.EL.DirectDriver()
  
  # process the job using the driver
  if args.clear and os.path.isdir(args.out):
    shutil.rmtree(args.out)

  driver.submit(job, args.out)


if __name__ == "__main__":
  main()
